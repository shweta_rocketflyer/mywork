package shw.springmvc.service;

import org.springframework.stereotype.Component;

import shw.springmvc.model.Login;
import shw.springmvc.model.User;

@Component
public interface UserService {

  int register(User user);

  User validateUser(Login login);
}
